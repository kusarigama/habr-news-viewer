const HTMLParser = require('node-html-parser');
const prompts = require('prompts');
const needle = require('needle');
const open = require('open');

exports.habrNews = function() {
  let url = 'https://habr.com/ru/all/';
  let interval;

  let p = new Promise((res, rej) => {
    let news = [];
    needle.get(url, function(error, response) {
      if (!error && response.statusCode == 200) {
        console.log('Searching for last habr news..');
        let dataHtml = response.body;
        let dataLinks = [];
        let dataTexts = [];
        dataHtml = HTMLParser.parse(dataHtml).querySelectorAll('h2.tm-article-snippet__title_h2 a');
        for (let link of dataHtml) {
          let titleLink = link.text;
          dataTexts.push(titleLink);
          link = link.getAttribute('href');
          dataLinks.push(link);
        }
        for (let i = 0; i < dataLinks.length; i++) {
          let ob = Object.create({});
          ob.title = dataTexts[i];
          ob.value = 'https://habr.com' + dataLinks[i];
          news.push(ob);
        }
      }
      res(news);
    });
  });

  p.then((news) => {
    (async function viewList() {
      let response = await prompts({
        type: 'select',
        name: 'value',
        message: 'Pick a news',
        choices: news
      }, {onCancel:cleanup, onSubmit:cleanup});
      open(response.value);
      viewList();
    })();
  });

  function cleanup() {
    clearInterval(interval);
  }
}
